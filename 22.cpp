#include <iostream>

using namespace std;

int main(){
    int a, i;
    cin>>a;
    if(a == 2){
        cout<<"YES"<<endl;
        return 0;
    }
    for(i = 2; i < a; i++){
        if(a % i == 0){
            cout<<"NO"<<endl;
            return 0;
        }
    }
    cout<<"YES"<<endl;

    return 0;
}