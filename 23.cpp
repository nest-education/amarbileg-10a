#include <iostream>


using namespace std;

int main(){
    int a, b, s, n, i;
    cin>>a>>b;

    if(a > b){
        if(a % b == 0){
            cout<<a<<endl;
            return 0;
        }
    }
    if(b > a){
        if(b % a == 0){
            cout<<b<<endl;
            return 0;
        }
        n = a;
        a = b;
        b = n;
    }
    s = a * b;
    for (i = 1; i < s; i++){
        if(i % a == 0 && i % b == 0){
            cout<<i<<endl;
            return 0;
        }

    }
    
    cout<<s<<endl;
    return 0;
}